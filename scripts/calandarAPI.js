var {google} = require('googleapis');

var key = require('./gleaming-lead-277215-d45be8ec874b.json');

const SCOPES = 'https://www.googleapis.com/auth/calendar';

var auth = new google.auth.JWT(
    key.client_email,
    null,
    key.private_key,
    SCOPES,
    'niteshkhanduja19943@gmail.com'
);

const api = google.calendar({version : "v3", auth : auth});
const calendarId = 'intern1.grail@gmail.com';

//Returns metadata for a calendar.
api.calendars.get({calendarId : calendarId}
    , function (err, resp) {
        if (err) {
            console.log(err);
        } else {
            console.log(resp);
        }
    })
         
//Creates a secondary calendar       
api.calendars.insert({requestBody : { summary : "test2"}},
     function (err, res) {
         if(err) {
             console.log(err);
         } else {
             console.log(res);
         }
     })

// Make an authorized request to list Calendar events.
    api.events.list({
        calendarId: calendarId
    }, function (err, resp) {
        if (err) {
            console.log(err)
        } else {
            console.log(resp.data.items);
        }
    });