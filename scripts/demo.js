/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const functions = require('firebase-functions');
const {google} = require('googleapis');
const {WebhookClient} = require('dialogflow-fulfillment');
const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'intern1.grail@gmail.com', // from where you want to send the Email
        pass: 'Codeischief#2020'         // password from that email id
    }
});


// Enter your calendar ID below and service account JSON below
const calendarId = "sr6s28hqr4o9r7v8lpat0dm5m0@group.calendar.google.com";
const serviceAccount = {
    "type": "service_account",
    "project_id": "gleaming-lead-277215",
    "private_key_id": "d45be8ec874b58158ab75d19dc9ed5e004842547",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDAOsuFU1hhWCUT\npgUlLxublQHIIzikuJP3CYHTJC4FLHHMv0WmADZYF5fNg+wIkxxboS/bFeCX4pEE\npeAgc1WrA0JufeLdW8o5qy+CGpjAoGafx341ZF2zE9u6YoOyYIbTe7juR9QmhDmb\njFuWHZF8m34d01TPbQiewADgzOfccBhNQifSVuylN0iY7OWr+mjzKC3nfTj/TYNc\n7L97XhweyOUVaU0oUDF5UDnBW0+xnJDFWhAHYVrCJYZGRji3oDY2UGHeRfUlYP9V\nfY4AKtqXGL7SiYZiWGBX42DyzJ1ZFARi3i+X5zM9d5EGjQXn03Pe4bxkNXuaTKuR\n0kzR1tSPAgMBAAECggEARXVZMbkuX6oqZ4GfLVLqvlpmlOsJHhfluycvxlu11z+6\ndPmOEI/WExmfS+7FnEmPvxosgRTUHP2MWeoX5JHYHB1m5SbOkR6IITNWTMQQetXa\njxRV6bP02W6tABl1rcAMJ6F2bn78zNgNOsPKGiFf/u7ucJd6dyLWMQp+prvTccZc\n2fkCs8TRrYoywHkUWJ0enUCAR8ZcURTRJsEs9rv7lscBJAPY636rKjcjxwrv2brn\n6776ry9ZxFxVJIZJCdUGB5h412zKSh2I8k80ThZcslZEvw0oJe36szx30bvTD84p\nXIBweNKGvEfJEEMdUuA+6OOdlxBOz6iAoJwigJI/3QKBgQDj+HV9KXbHWZb8YwuR\n+K+vyKwf0o2s83srBOqEYoJ5+mmYvjnzURsTH4SxzBnbz7TLFKuK02jibZ1URDkL\nepljbCN3TImkmQy4g19cfiu1K263UWycG57+TwEqjjmbcH2O3tM6yB9SlNcI7wkD\na35PJtKbQNFDl37Iv71Jg/K67QKBgQDX3V2bjeRiTnRYGEhakCAcZc5xLe2jU9Az\nu6QS3zSnr2Jwh14sGlAvwGwJL44JNm43Zk5Ve7sZUE81EwkiBAb1xB8bQB1Bl73e\nS4DAkmfDNErxpbUzoAjOkLH22JjmxGWarkmMwyoy/oLzPCrBsUr6eEKasMSaFb2x\nr3EwKRmR6wKBgQCFojK08OZoy1KflivFIKNUBu0nJlTYak3sFNCoU9qqSCk4WWI9\nCHE3j4VIeB08ZhBT70TzdyQHfzv3lppd6TsbNdvAZgRVkUYMry3qWSej5tba7Qce\np25XItbLLF4GiTWA14u47HC4BvnaicydzZhYK6jPS3qgBaFTalo6SvexrQKBgQDX\nvHsDLcoknbgVGx+kswoEl5K89mPbqbYZHd0ap0zekZ/WStPJS/r73lmazJYtiuKY\nJ3z+Xgxdfi/LIV0tS80Q7pMQDwGuPlMZyWfCDg/5vVdxLmVsVPK4T1AtHceiSoIp\nFyqWra6NDGQfZ/BraB9BN9OI+kVioXvWCzsaINlXRQKBgQDLVPRNxXiIfNrhwp6i\n8RaadbtoygqROj+3wiFIZcJ9+Wln3b6L9QsH/n8T6LEPcPjrRcVOw0DZDaYeJ+FT\nlJujroO6OkLHt6nLN/6FLACgC257sIpSnZBWNcI5N8hLL+o2KjcVNf4lho/LwZK0\n5aGSXcRGqSAZBGP7I/giR+H1og==\n-----END PRIVATE KEY-----\n",
    "client_email": "calendar-api@gleaming-lead-277215.iam.gserviceaccount.com",
    "client_id": "108048804764415477955",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/calendar-api%40gleaming-lead-277215.iam.gserviceaccount.com"
  }
  ; // Starts with {"type": "service_account",...

// Set up Google Calendar Service account credentials
const serviceAccountAuth = new google.auth.JWT({
  email: serviceAccount.client_email,
  key: serviceAccount.private_key,
  scopes: 'https://www.googleapis.com/auth/calendar'
});

const calendar = google.calendar('v3');
process.env.DEBUG = 'dialogflow:*'; // enables lib debugging statements

const timeZone = 'America/Los_Angeles';
const timeZoneOffset = '-07:00';

exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log("Parameters", agent.parameters);
  const appointment_type = agent.parameters.AppointmentType;
  function makeAppointment (agent) {
    // Calculate appointment start and end datetimes (end = +1hr from start)
    //console.log("Parameters", agent.parameters.date);
    const dateTimeStart = new Date(Date.parse(agent.parameters.date.split('T')[0] + 'T' + agent.parameters.time.split('T')[1].split('-')[0] + timeZoneOffset));
    const dateTimeEnd = new Date(new Date(dateTimeStart).setHours(dateTimeStart.getHours() + 1));
    const appointmentTimeString = dateTimeStart.toLocaleString(
      'en-US',
      { month: 'long', day: 'numeric', hour: 'numeric', timeZone: timeZone }
    );

    // Check the availibility of the time, and make an appointment if there is time on the calendar
    return createCalendarEvent(dateTimeStart, dateTimeEnd, appointment_type).then(() => {
      agent.add(`Ok, let me see if we can fit you in. ${appointmentTimeString} is fine!.`);
    }).catch(() => {
      agent.add(`I'm sorry, there are no slots available for ${appointmentTimeString}.`);
    });
  }
  function sendEmailHandler(agent){
   const{email,name} = agent.parameters;
    const mailOptions = {
    from: "CodeMischief.io", // sender address
    to: email, // list of receivers
    subject:"Appointent with Us", // Subject line
    html: `<p> Here is yout Appointent for ${name} </p>`
};
    transporter.sendMail(mailOptions, function (err, info) {
    if(err)
    {
      console.log(err);
    }
});
  }

  
  
  
  let intentMap = new Map();
  intentMap.set('Appointment', makeAppointment);
  
  intentMap.set('Appointment',sendEmailHandler);
  agent.handleRequest(intentMap);
});



function createCalendarEvent (dateTimeStart, dateTimeEnd, appointment_type) {
  return new Promise((resolve, reject) => {
    calendar.events.list({
      auth: serviceAccountAuth, // List events for time period
      calendarId: calendarId,
      timeMin: dateTimeStart.toISOString(),
      timeMax: dateTimeEnd.toISOString()
    }, (err, calendarResponse) => {
      // Check if there is a event already on the Calendar
      if (err || calendarResponse.data.items.length > 0) {
        reject(err || new Error('Requested time conflicts with another appointment'));
      } else {
        // Create event for the requested time period
        calendar.events.insert({ auth: serviceAccountAuth,
          calendarId: calendarId,
          resource: {summary: appointment_type +' Appointment', description: appointment_type,
            start: {dateTime: dateTimeStart},
            end: {dateTime: dateTimeEnd}}
        }, (err, event) => {
          err ? reject(err) : resolve(event);
        }
        );
      }
    });
  });
}